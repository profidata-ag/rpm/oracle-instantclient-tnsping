%global debug_package %{nil}

Name:     oracle-instantclient-tnsping
Version:  1.0.0
Release:  1%{?dist}
Summary:  TNSping utility

License:  MIT
URL:      https://gitlab.com/profidata-ag/rpm/oracle-instantclient-tnsping

Source0:  tnsping
Requires: nmap-ncat, netcat

%description
TNSping is a shell script that emulates the tnsping utility from Oracle.

%prep
# nothing to do

%build
# nothing to do

%install
install -D %{SOURCE0} %{buildroot}%{_bindir}/tnsping

%files
%{_bindir}/tnsping
